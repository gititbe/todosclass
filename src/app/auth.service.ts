import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  signup(email:string,password:string){
   return this.fireBaseAuth
    .auth
    .createUserWithEmailAndPassword(email,password);
  }

updateProfile(user,name:string){
  user.updateProfile({displayName:name,photoURL:''});
}
  constructor(private fireBaseAuth:AngularFireAuth) { }
}