import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList } from '@angular/fire/database'
@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
todoTextfromTodo ='No text so far';


showText($event){
  this.todoTextfromTodo = $event;
}

todos =[];
  constructor(private db:AngularFireDatabase) { }

  ngOnInit() {
    this.db.list('/todos').snapshotChanges().subscribe(
      todos => {
        this.todos = [];
        todos.forEach(
          todo =>{
            let y = todo.payload.toJSON();
            y['$key'] = todo.key;
            this.todos.push(y);
          }
        )

      }
    )

  }

}
