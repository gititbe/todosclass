import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database'

@Component({
  selector: 'app-user-todos',
  templateUrl: './user-todos.component.html',
  styleUrls: ['./user-todos.component.css']
})
export class UserTodosComponent implements OnInit {

user = 'jack'
todoTextfromTodo ='No text so far';

showText($event){
  this.todoTextfromTodo = $event;
}

todos =[];
  constructor(private db:AngularFireDatabase) { }

  changeuser(){
    this.db.list('/users/'+this.user + '/todos').snapshotChanges().subscribe(
      todos => {
        this.todos = [];
        todos.forEach(
          todo =>{
            let y = todo.payload.toJSON();
            y['$key'] = todo.key;
            this.todos.push(y);
          }
        )

      }
    )

  

  }

  ngOnInit() {
    this.db.list('/users/'+this.user + '/todos').snapshotChanges().subscribe(
      todos => {
        this.todos = [];
        todos.forEach(
          todo =>{
            let y = todo.payload.toJSON();
            y['$key'] = todo.key;
            this.todos.push(y);
          }
        )

      }
    )

  }

}

