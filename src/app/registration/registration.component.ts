import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  email: string;
  password: string;
  name: string;
  error = '';
  


  signup(){
   // console.log("sign up clicked" +' '+ this.email+' '+this.password+' '+this.name)
    this.authService.signup(this.email,this.password)
      .then(value=>{
         this.authService.updateProfile(value.user,this.name);
      }).then(value=>{
        this.router.navigate(['/']);
      }).catch(err => { 
        this.error = err;
        console.log(err);
      })
  }

  constructor(private authService:AuthService,private router:Router ) { }

  ngOnInit() {
  }

}